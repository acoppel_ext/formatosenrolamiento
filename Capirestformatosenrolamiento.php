<?php
class Capirestformatosenrolamiento {

	function consumirApi($sFuncion, $arrDatos, $controlador)
	{
		//IP Server
		$host = $_SERVER["HTTP_HOST"];
		$xml = simplexml_load_file("../conf/apiconfig.xml");
		$ipServidor = "";
		foreach($xml->item as $elemento)
		{
			if( $host == $elemento->id){
				$ipServidor = $elemento->value;
			}
		}
		if($ipServidor == ""){
			$ipServidor = $xml->default;
		}
		$url = 'http://'.$ipServidor.'/apirestafiliacion/codeigniter/api/'.$controlador.'/'.$sFuncion.'/';

		//API key
		$apiKey = '1234';

		//Auth credentials
		$username = "admin";
		$password = "1234";

		//Solicitar Token 
		$requiereToken = false;
		$componenteActual = "formatosenrolamiento";
		foreach($xml->componentes as $componente)
		{
			if( $componenteActual  == $componente->id){
				$requiereToken = $componente->value;
			}
		}

		$arrDatos["componente"] = $requiereToken === 'true'? true: false;

		//create a new cURL resource
		$ch = curl_init($url);

		curl_setopt($ch, CURLOPT_TIMEOUT, 30);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
		if(isset($_SESSION["authorization"])){
			curl_setopt($ch, CURLOPT_HTTPHEADER, array("X-API-KEY: " . $apiKey, "Authorization:" . $_SESSION["authorization"]));
		}else{
			curl_setopt($ch, CURLOPT_HTTPHEADER, array("X-API-KEY: " . $apiKey));
		}
		curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $arrDatos);

		$result = curl_exec($ch);

		//close cURL resource
		curl_close($ch);

		if($result)
			$result = json_decode($result,true);

		return $result;
	}
}
?>