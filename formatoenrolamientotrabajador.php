<?php
	include("../fpdf/code39.php");
	include("../fpdi/fpdi.php");
	include("clases/CMetodosExpedienteAfiliacion.php");
	include_once ("clases/CLogImpresion.php");
	date_default_timezone_set('America/Mazatlan');


$objGn = new CMetodosExpedienteAfiliacion();
$cnxBd = null;
$iCount = 0;
$iContinuar = 0;

$arrErr = array();
$arrResp = array();
$arrResp_Leyenda = array();
$arrFolio = array();

$cnxBd =  new PDO( "pgsql:host=".IPSERVIDOR.";port=5432;dbname=".BASEDEDATOS, USUARIO, PASSWORD);
if($cnxBd)
{
	CLogImpresion::escribirLog("Accedio a la base de datos");
	if(isset($_GET['folioenrolamiento']))
	{
		CLogImpresion::escribirLog("Parametro enviado correctamente");
		$iFolioEnrolamiento =$_GET['folioenrolamiento'];

		$sSql = "";
		$sSql = "SELECT COUNT(*) FROM fnconsultatrabajadorenrolamiento(".$iFolioEnrolamiento.")";
		CLogImpresion::escribirLog( $sSql );
		
		$resulSet = $cnxBd->query($sSql);
		if($resulSet)
		{	
			$iCount = 0;		
			$iCount =  $resulSet->fetchColumn();
			if($iCount > 0)
			{
				$iContinuar = 1;
				CLogImpresion::escribirLog("Registros encontrados");
			}
		}
		else
		{
			$arrErr = $cnxBd->errorInfo();
			echo ' Error: ' . $arrErr[0] . '-' . $arrErr[1] . '-' . $arrErr[2];
			CLogImpresion::escribirLog(' Error: ' . $arrErr[0] . '-' . $arrErr[1] . '-' . $arrErr[2]);
		}
		if($iContinuar > 0)
		{
			$sSql = "";
			$sSql = "SELECT folio, tiposolicitud, curp, nombre, apellidopa, apellidoma, texto 
					 FROM fnconsultatrabajadorenrolamiento(".$iFolioEnrolamiento.");";

			CLogImpresion::escribirLog( $sSql );
			
			$resulSet = $cnxBd->query($sSql);
			if($resulSet)
			{	
				//Se cargan los datos del enrolado
				foreach($resulSet as $reg)
				{
					$arrDatosFormatoEnrolamiento  = array(
									'folio' => $reg['folio'],
									'tiposolicitud' => $reg['tiposolicitud'],
									'curp' => $reg['curp'],
									'nombre' => strpos($reg['nombre'], '�') || strpos($reg['nombre'], '�') ? $reg['nombre'] : utf8_encode($reg['nombre']),
									'paterno' => strpos($reg['apellidopa'], '�') || strpos($reg['apellidopa'], '�') ? $reg['apellidopa'] : utf8_encode($reg['apellidopa']),
									'materno' => strpos($reg['apellidoma'], '�') || strpos($reg['apellidoma'], '�') ? $reg['apellidoma'] : utf8_encode($reg['apellidoma']),
									'texto' => $reg['texto'],
									);
				//FORMATO VACIO
				CLogImpresion::escribirLog("Se lleno arreglo correctamente");
				$arrDatosFormatoEnrolamientoNew = array_map('trim', $arrDatosFormatoEnrolamiento);
				}// END FOREACH
			}
			else
			{
				$arrErr = $cnxBd->errorInfo();
				echo ' Error: ' . $arrErr[0] . '-' . $arrErr[1] . '-' . $arrErr[2];
				CLogImpresion::escribirLog(' Error: ' . $arrErr[0] . '-' . $arrErr[1] . '-' . $arrErr[2]);
			}
		} //VALIDAR EXISTE FOLIO
	}
	else
	{
		echo 'parametros incorrectos<br>';
		CLogImpresion::escribirLog('parametros incorrectos<br>');

	}

	if($iContinuar > 0)
	{
		$arrRuta = array("rutapdf");
		$arrRuta["rutapdf"] = generarFormatoEnrolamientoTrabajador($arrDatosFormatoEnrolamiento, $iFolioEnrolamiento);
		//echo json_encode($arrRuta);
	}
	else
	{
			echo ' Excepcion! El Empleado Solicitado no existe';
			CLogImpresion::escribirLog(' Excepcion! El Empleado Solicitado no existe');
	}
}
else
{
	echo 'Error, al abrir conexion a BD postgres';
	CLogImpresion::escribirLog('Error, al abrir conexion a BD postgres');
}

//Metodo para generar el Formato de Autorizacion de Huellas Dactilares en PDF
function generarFormatoEnrolamientoTrabajador($arrDatosFormatoEnrolamiento, $iFolioEnrolamiento)
{
	$cnxBd =  new PDO( "pgsql:host=".IPSERVIDOR.";port=5432;dbname=".BASEDEDATOS, USUARIO, PASSWORD);
	
	$iTipoExcepcion = 0;
	$iTipoExcepcion = obtenerExcepcionenrol( $iFolioEnrolamiento );
	
	$respuesta = new stdClass();
	$pdf = new PDF_Code39();
	$pdf->AliasNbPages();
	$pdf->AddPage('P','Letter');
	$iAltoRen = 5.5;
	$iSaltoLinea = 5;
	$sizeFontCuerpo = 12;
	$iRadio = 1;
	$iBorder = 0;
	$InicioMargen = 4;
	$FinMargen = 208;
	$SangriaMargen = 11;
	$iPidActual = getmypid();
	$nombre = utf8_decode(trim($arrDatosFormatoEnrolamiento['nombre']));
	$apellidop = utf8_decode(trim($arrDatosFormatoEnrolamiento['paterno']));
	$apellidom = utf8_decode(trim($arrDatosFormatoEnrolamiento['materno']));
	$sNombreCompleto = $nombre." ". $apellidop." ". $apellidom;
	$iTotalCaracteres = strlen($sNombreCompleto);
	$iTipoSolicitud = 0;

	$arrayMeses = array('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
   'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');

	CLogImpresion::escribirLog("Entro a generarFormatoEnrolamientoTrabajador");

	//Decodificar el arreglo para mostrar correctamente los acentos
	foreach ($arrDatosFormatoEnrolamiento as $key=>$valor)
	{
		$arrDatosFormatoEnrolamiento[$key] = $arrDatosFormatoEnrolamiento[$key];
	}
	//Establecer margenes izquierdo, arriba, derecho
	$pdf->SetMargins(20, 20 ,20);
	//Establecemos el margen inferior:
	$pdf->SetAutoPageBreak(true,0);

	$pdf->AddFont('Tahoma','','tahoma.php');
	
	$pdf->SetTextColor(0,0,0);
	$pdf->Image('../imagenes/LogoAfore.jpg',17,55,60,14);
	$pdf->SetTextColor(18,50,137);
	$pdf->SetXY(17,67);
	$pdf->SetFont('Arial','B',$sizeFontCuerpo-3.8);
	$pdf->Cell( 216, $iAltoRen, 'Efectivo para su retiro' , $iBorder, 0, 'L');
	$pdf->SetTextColor(19,111,191);
	$pdf->SetXY( 9,20);
	$pdf->SetFont('Arial','B',$sizeFontCuerpo+4);
	$pdf->Cell( 216, $iAltoRen, 'Formato de Enrolamiento con la firma manuscrita digital' , $iBorder, 0, 'L');
	$pdf->RoundedRect(10, 40, 196, 44, 5, 'D','1,2,3,4' );
	$pdf->RoundedRect(10, 90, 196, 116, 5, 'D','1,2,3,4' );

	$pdf->setX($InicioMargen);
	$pdf->Ln($iSaltoLinea*2+1);
	
	$pdf->SetFont('Tahoma','B',$sizeFontCuerpo-1);
	$pdf->SetXY(87,53);
	$pdf->SetTextColor(0,0,0);
	$pdf->Cell( 57, $iAltoRen, 'Clave de la administradora' , $iBorder, 0, 'L');
	$pdf->SetXY(87,68);
	$pdf->Cell( 57, $iAltoRen, 'Nombre de la administradora' , $iBorder, 0, 'L');
	$pdf->SetXY(147,53);
	$pdf->SetFont('Tahoma','',$sizeFontCuerpo+1);
	$pdf->Cell( 57, $iAltoRen, '568' , $iBorder, 0, 'L');
	$pdf->SetXY(147,68);
	$pdf->Cell( 57, $iAltoRen, 'Afore Coppel' , $iBorder, 0, 'L');
	$pdf->SetFont('Tahoma','B',$sizeFontCuerpo-1);
	$pdf->SetXY(17,105);
	$pdf->Cell( 57, $iAltoRen, 'CURP del Trabajador' , $iBorder, 0, 'L');
	$pdf->SetXY(17,120);
	$pdf->Cell( 57, $iAltoRen, 'Nombre del Trabajador' , $iBorder, 0, 'L');
	$pdf->SetXY(17,135);
	$pdf->Cell( 57, $iAltoRen, 'Fecha del Proceso de la Solicitud' , $iBorder, 0, 'L');
	$pdf->SetTextColor(0,0,0);
	$pdf->SetFont('Arial','',$sizeFontCuerpo-4);
	$pdf->SetXY( $InicioMargen+13,147);
	$pdf->MultiCell(($FinMargen +$InicioMargen) -35, 5,utf8_decode(trim($arrDatosFormatoEnrolamiento['texto'])), $iBorder, 'J');
	if ($iTotalCaracteres <= 39){
		$pdf->SetFont('Arial','',$sizeFontCuerpo);
		$pdf->SetXY( 100,120);
		$pdf->Cell( 216,$iAltoRen, $sNombreCompleto);
		$pdf->SetXY( 100,106);
		$pdf->Cell( 216, $iAltoRen, trim($arrDatosFormatoEnrolamiento['curp']) , $iBorder, 0, 'L');
		$pdf->SetXY( 100,135);
		$pdf->Cell(39, $iAltoRen, /*utf8_decode(trim($arrDatosFormatoEnrolamiento['ciudad'])).', '.utf8_decode(trim($arrDatosFormatoEnrolamiento['estado'])).', '.*/date('d')." de ".$arrayMeses[date('m')-1]." de ".date('Y'), $iBorder/**/, 0, 'L');
	}
	else if ($iTotalCaracteres > 39 && $iTotalCaracteres <= 48){
		$pdf->SetFont('Arial','',$sizeFontCuerpo - 2);
		$pdf->SetXY( 100,120);
		$pdf->Cell( 216, $iAltoRen, $sNombreCompleto);
		$pdf->SetFont('Arial','', $sizeFontCuerpo);
		$pdf->SetXY( 100,106);
		$pdf->Cell( 216, $iAltoRen, trim($arrDatosFormatoEnrolamiento['curp']) , $iBorder, 0, 'L');
		$pdf->SetXY( 100,135);
		$pdf->Cell(39, $iAltoRen, /*utf8_decode(trim($arrDatosFormatoEnrolamiento['ciudad'])).', '.utf8_decode(trim($arrDatosFormatoEnrolamiento['estado'])).', '.*/date('d')." de ".$arrayMeses[date('m')-1]." de ".date('Y'), $iBorder/**/, 0, 'L');
	}	
	else {
		$pdf->SetFont ('Arial','',$sizeFontCuerpo - 2);
		$pdf->SetXY(86,120);
		$pdf->Cell(216, $iAltoRen, $sNombreCompleto);
		$pdf->SetFont('Arial','', $sizeFontCuerpo);
		$pdf->SetXY( 86,106);
		$pdf->Cell( 216, $iAltoRen, trim($arrDatosFormatoEnrolamiento['curp']) , $iBorder, 0, 'L');
		$pdf->SetXY( 86,135);
		$pdf->Cell(39, $iAltoRen, /*utf8_decode(trim($arrDatosFormatoEnrolamiento['ciudad'])).', '.utf8_decode(trim($arrDatosFormatoEnrolamiento['estado'])).', '.*/date('d')." de ".$arrayMeses[date('m')-1]." de ".date('Y'), $iBorder/**/, 0, 'L');
	}
	
	$pdf->SetXY( 145,240);
	$pdf->Cell( 52, $iAltoRen, '______________________' , $iBorder, 0, 'L');

	if( $iTipoExcepcion == 4 )
	{
		$pdf->SetXY( 145,240);
		$pdf->SetFont('Arial','B',$sizeFontCuerpo-2);
		$pdf->Cell( 43, $iAltoRen, 'AMBAS MANOS AMPUTADAS' , $iBorder, 0, 'L');
	}
	else if( $iTipoExcepcion == 3 )
	{
		$pdf->SetXY( 145, 240);
		$pdf->SetFont('Arial','B',$sizeFontCuerpo-2);
		$pdf->Cell( 43, $iAltoRen, 'AMBAS MANOS LESIONADAS' , $iBorder, 0, 'L');
	}

	$pdf->SetXY( 150,246);
	$pdf->SetFont('Arial','B',$sizeFontCuerpo-2);
	$pdf->Cell( 43, $iAltoRen, 'Firma manuscrita digital' , $iBorder, 0, 'L');

	//BORDES
	$pdf->SetFillColor(8,52,99);
	$pdf->SetXY( 0,0);
	$pdf->Cell( 216, $iAltoRen-.5, '' , 1, 0, 'C',true);
	$pdf->SetFillColor(8,52,99);
	$pdf->SetXY( 0,274.3);
	$pdf->Cell( 216, $iAltoRen-.5, '' , 1, 0, 'C',true);
	
	$iTipoSolicitud = $arrDatosFormatoEnrolamiento['tiposolicitud'];
	CLogImpresion::escribirLog("Folio = ".$arrDatosFormatoEnrolamiento['folio']);
	CLogImpresion::escribirLog("Tipo Solicitud = ".$iTipoSolicitud);
	
	if($iTipoSolicitud != 26 && $iTipoSolicitud != 27 && $iTipoSolicitud != 33)
	{
		$arrDatosFormatoEnrolamiento['folio'] = $arrDatosFormatoEnrolamiento['folio']."-S";
		CLogImpresion::escribirLog("Es un folio Servicio = ".$arrDatosFormatoEnrolamiento['folio']);
	}
	
	$rutapdf = "/sysx/progs/web/salida/formatosenrolamiento/".$arrDatosFormatoEnrolamiento['folio']."_".$iPidActual."_FormatoEnrolTrab".trim($arrDatosFormatoEnrolamiento['curp']).".pdf";
	$rutaa = "/salida/formatosenrolamiento/".$arrDatosFormatoEnrolamiento['folio']."_".$iPidActual."_FormatoEnrolTrab".trim($arrDatosFormatoEnrolamiento['curp']).".pdf";
	$sRutaAbsoluta = $arrDatosFormatoEnrolamiento['folio']."_".$iPidActual."_FormatoEnrolTrab".trim($arrDatosFormatoEnrolamiento['curp']).".pdf";
	$respuesta->rutapdf = "/sysx/progs/web/salida/formatosenrolamiento/".$arrDatosFormatoEnrolamiento['folio']."_".$iPidActual."_FormatoEnrolTrab".trim($arrDatosFormatoEnrolamiento['curp']).".pdf";
	$rutaPdfTemp = "/sysx/progs/web/salida/formatosenrolamiento/".$arrDatosFormatoEnrolamiento['folio']."_".$iPidActual."_FormatoEnrolTrab".trim($arrDatosFormatoEnrolamiento['curp'])."_TEMP.pdf";
	
	CLogImpresion::escribirLog("Se guarda PDF sin firma");
	$objMetodosExpedienteAfiliacion = new CMetodosExpedienteAfiliacion();	
	CLogImpresion::escribirLog("Se Abre PDF para agregar firma del Enrolado");


	$pdf->Output($rutapdf); // Se guarda el formato enrolamiento
	$arrResp['respuesta'] 	= 1;
	$arrResp['mensaje'] 	= 'Exito';
	$arrResp['rutapdf']	    = $rutapdf;
	echo json_encode($arrResp);
	CLogImpresion::escribirLog("Se guarda PDF sin firma");
	$iTieneExecp = 0;//Validar en la afiliacion si este tiene exepcion de
	//RUTA DE LA LEYENDA DEL ENROLADO
	if ( $iTipoExcepcion == 4 || $iTipoExcepcion == 3 ) { // Si el enrolamiento se hace con alguna excepcion AML � AMA entonces despues de guardar el formato enrolamiento se pega la leyeda de la excepcion
		unset($arrResp_Leyenda);
		if($iTipoSolicitud != 26 && $iTipoSolicitud != 27 && $iTipoSolicitud != 33){
			$sRutaFirmaEnrolado = "/sysx/progs/web/entrada/firmas/FAHD_".$arrDatosFormatoEnrolamiento['folio']."_LTRAB.JPG";
			$arrResp_Leyenda = $objMetodosExpedienteAfiliacion->unirImagenLeyendaExecepcion($rutapdf, $sRutaFirmaEnrolado);
			if($arrResp_Leyenda["estado"] == OK___) {
				CLogImpresion::escribirLog(" Exito al agregar la la leyenda de la excepcion ");
			}
		}else{
			$iTieneExecp = 1;
		}
		
	}
	
	CLogImpresion::escribirLog("Se Abre PDF para agregar firma del Enrolado");
	copy($rutapdf, $rutaPdfTemp);
	$pdf = new FPDI();

	chmod($rutapdf, 0777);
	unset($arrResp);

	//RUTA DE LA FIRMA DEL ENROLADO
	if($iTipoSolicitud != 26 && $iTipoSolicitud != 27 && $iTipoSolicitud != 33){
		$sRutaFirmaEnrolado = "/sysx/progs/web/entrada/firmas/FAHD_".$arrDatosFormatoEnrolamiento['folio']."_FTRAB.JPG";
	}else{
		if($iTieneExecp == 0){
			$sRutaFirmaEnrolado = "/sysx/progs/web/entrada/firmas/UFAF_".$arrDatosFormatoEnrolamiento['folio']."_FTRAB.JPG";
		}else{
			$sRutaFirmaEnrolado = "/sysx/progs/web/entrada/firmas/UFAF_".$arrDatosFormatoEnrolamiento['folio']."_FPROM.JPG";
		}
	}

	$arrResp = $objMetodosExpedienteAfiliacion->unirImagenesFormatoEnrol($rutapdf, $sRutaFirmaEnrolado);

	if($arrResp["estado"] == OK___)
	{
		$pageCount = $pdf->setSourceFile('/sysx/progs/web/salida/formatosenrolamiento/'.$sRutaAbsoluta);
		$tplIdx = $pdf->importPage(1);
		$pdf->addPage();
		$pdf->useTemplate($tplIdx, null, null, 0, 0, true);
		
		CLogImpresion::escribirLog("Se agrega la firma al PDF");
	}
	

	unset($arrResp);
	unset($arrFolio);
	$arrFolio = explode("-", $arrDatosFormatoEnrolamiento['folio']);
	
	//EN BASE AL TIPO DE SOLICITUD SE ACTUALIZARA LA TABLA CORRESPONDIENTE
	if($iTipoSolicitud != 26 && $iTipoSolicitud != 27 && $iTipoSolicitud != 33)
	{
		//SE ACTUALIZA EN LA TABLA STMPPUBLICARIMAGENSERVICIOS
		$arrResp = $objMetodosExpedienteAfiliacion->actualizarpublicarimagenservicios($arrFolio[0], 2);
	}
	else
	{
		//CLogImpresion::escribirLog("SE MANDA LLAMAR EL METODO DE ACTUALIZARPUBLICARIMAGEN");
		//SE ACTUALIZA EN LA TABLA STMPPUBLICARIMAGEN
		//$arrResp = $objMetodosExpedienteAfiliacion->actualizarpublicarimagen($arrFolio[0], 6);
		
		//SE PASA EL NOMBRE DEL ARCHIVO A LA FUNCION ctrlImagenesPorTransmitir.
		$cNombreArchivo = $arrDatosFormatoEnrolamiento['folio']."_".$iPidActual."_FormatoEnrolTrab".trim($arrDatosFormatoEnrolamiento['curp']).".pdf";
		
		//SE ACTUALIZA EN LA TABLA "ctrlImagenesPorTransmitir".
		$arrResp = $objMetodosExpedienteAfiliacion->ctrlImagenesPorTransmitir(6,$arrFolio[0],$cNombreArchivo);
		CLogImpresion::escribirLog("Se registraron los datos del PDF en ctrlImagenesPorTransmitir correctamente.");
	}
	
	//SE GUARDA PDF Y SE MUESTRA EN EL NAVEGADOR
	$pdf->Output($rutapdf, 'F');
	$arrResp['respuesta'] 	= 1;
	$arrResp['mensaje'] 	= 'Exito';
	$arrResp['rutapdf']	    = $rutapdf;
	//$pdf->Output($rutapdf, 'I');
	
	CLogImpresion::escribirLog("Se guarda y se muestra PDF con la firma del Trabajador Enrolado en el navegador");

	return $rutaa;	
}

function obtenerExcepcionenrol( $iFolioEnrolamiento )
{
	$cnxBd =  new PDO( "pgsql:host=".IPSERVIDOR.";port=5432;dbname=".BASEDEDATOS, USUARIO, PASSWORD);
	$sSql = "";
	$iRespuesta = 0;
	if( $cnxBd )
	{
		$sSql = " SELECT fnobtenerexcepcionbitacoraenrol AS iexcepcion FROM fnobtenerexcepcionbitacoraenrol(". $iFolioEnrolamiento .", ". 1 ."); ";
		CLogImpresion::escribirLog("[".__FUNCTION__."] Funcion [". $sSql ."] ");
		$resulSet = $cnxBd->query($sSql);
		if($resulSet)
		{
			foreach($resulSet as $reg)
			{
				$iRespuesta = $reg["0"];
			}
			CLogImpresion::escribirLog("[".__FUNCTION__."] Resultado de la funcion : [". $iRespuesta ."] ");
			if ( $iRespuesta == -1)
			{
				CLogImpresion::escribirLog("[".__FUNCTION__."] No se encontro registro en la bitacora con el folio [". $iFolioEnrolamiento ."] ");
			}
			else if ( $iRespuesta == 0 )
			{
				CLogImpresion::escribirLog("[".__FUNCTION__."] Excepcion no corresponde a [Ambas Manos Lesionadas o Amputadas] bitacora con el folioenrolamiento [". $iFolioEnrolamiento ."] ");
			}
			else
			{
				CLogImpresion::escribirLog("[".__FUNCTION__."] Excepcion valida en la bitacora con el folio [". $iFolioEnrolamiento ."] ");
			}
		}
	}
	else
	{
		CLogImpresion::escribirLog("[".__FUNCTION__."] Error: Se intento abrir la conexion a la BD ");
	}

	return $iRespuesta;
}

function obtIpTienda()
{
	$ipCliente = "";
	
	if(isset($_SERVER["HTTP_X_FORWARDED_FOR"]) )
		$ipCliente = $_SERVER['HTTP_X_FORWARDED_FOR']; 
	else
		$ipCliente = $_SERVER['REMOTE_ADDR'];

	return $ipCliente;
}

?>