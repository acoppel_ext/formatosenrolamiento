<?php
include_once ("global.php");
include_once ("CLogImpresion.php");
define('RUTA_LOGX',					'/sysx/progs/afore/log/metodosFormatoEnrolamiento');
define("RUTA_SALIDA","/sysx/progs/web/salida/");

class CMetodosExpedienteAfiliacionMovil
{
	function __construct()
	{
		date_default_timezone_set('America/Mazatlan');
	}
	function __destruct()
	{
	}
	
	var $cnxDb;
	var $arrError;

	public function grabarLogx($cadLogx)
	{
		$cIpCliente = $this->getRealIP();
		$rutaLog =  RUTA_LOGX .  '-' . date("Y-m-d") . ".log"; 
		$cad = date("Y-m|H:i:s|") . getmypid() . "|" . $cIpCliente . "| " . $cadLogx . "\n";
		$file = fopen($rutaLog, "a");
		if( $file )
		{
			fwrite($file, $cad);
		}
		fclose($file);
	}

	public function getRealIP() 
	{
		if (!empty($_SERVER['HTTP_CLIENT_IP']))
		return $_SERVER['HTTP_CLIENT_IP'];
		
		if (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
		return $_SERVER['HTTP_X_FORWARDED_FOR'];
		
		return $_SERVER['REMOTE_ADDR'];
	}

	public function obtenerConexionBdPostgres($DireccionIp, $BaseDato, $UsrBaseDato, $PasswdBaseDato)
	{
		global $cnxDb;
		if( pg_connection_status($cnxDb) != PGSQL_CONNECTION_OK )
		{
			$cadConexion = "host=" . $DireccionIp . " dbname=" . $BaseDato . " user=" . $UsrBaseDato . " password=" . $PasswdBaseDato;
			$cnxDb = pg_pconnect($cadConexion);
			if( $cnxDb )
			$this->grabarLogx('[obtenerConexionBdPostgres] Conexion establecida');
		}
		
		return $cnxDb;
	}

	//METODO PARA UNIR LA IMAGEN DEL DOCUMENTO CON LAS FIRMAS
	function unirImagenesFormatoEnrol($sRutaImagenDocumento, $sRutaImagenFirmaEnrolado)
	{
		$arrResp = array("estado" => -1, "descripcion" => '');
		
		try
		{
			//Se crean tres objetos
			$imagenDocumento = new Imagick($sRutaImagenDocumento);
			$imagenFirmaEnrolado = new Imagick($sRutaImagenFirmaEnrolado);
			self::grabarLogx("[CMetodosExpedienteAfiliacion::unirImagenesFormatoEnrol] Ruta del documento-> ".$sRutaImagenDocumento);
			self::grabarLogx("[CMetodosExpedienteAfiliacion::unirImagenesFormatoEnrol] Ruta de la imagen Firma-> ".$sRutaImagenFirmaEnrolado);
			//SE ASIGNA LA RESOLUCION DEL PDF
			$imagenDocumento->setResolution(200,200);
			
			//SE LEE EL PDF Y LAS IMAGENES A ADJUNTAR
			$imagenDocumento->readImage($sRutaImagenDocumento);
			$imagenFirmaEnrolado->readImage($sRutaImagenFirmaEnrolado);
			
			$imagenDocumento->setImageFormat('jpeg');
			$imagenDocumento->setCompression(Imagick::COMPRESSION_JPEG);
			$imagenDocumento->setCompressionQuality(100);
			
			//Se modifica el tamaño de la firma
			$imagenFirmaEnrolado->resizeImage(556, 300, Imagick::FILTER_BOX, 1);
			
			//La segunda imagen se pone arriba de la primera imagen
			$imagenDocumento->compositeImage($imagenFirmaEnrolado, Imagick::COMPOSITE_BUMPMAP, 1084, 1701);
			
			if($imagenDocumento->writeImage($sRutaImagenDocumento))
			{
				self::grabarLogx("[CMetodosExpedienteAfiliacion::unirImagenesFormatoEnrol] SE UNIERON IMAGENES CON EXITO");
				$arrResp["estado"] = OK___;
			}
			else{
				self::grabarLogx("[CMetodosExpedienteAfiliacion::unirImagenesFormatoEnrol] ERROR AL UNIR FIRMAS");
			}
			return $arrResp;
		}catch(ImagickException $e)
		{
			$arrError = $e->getTrace();
			$arrResp["descripcion"] = 'Ruta y nombre del PDF: ['.$arrError[1]["args"][0].'] Ruta y nombre de la firma: ['.$arrError[1]["args"][1].']';
			self::grabarLogx('[CMetodosExpedienteAfiliacion::unirImagenesFormatoEnrol] Excepcion: '.$e->getMessage().' Linea: '.$e->getLine().'  Codigo: '.$e->getCode());
			return $arrResp;
			
		}
	}

	//METODO PARA UNIR LA IMAGEN DEL DOCUMENTO CON LAS FIRMAS
	function unirImagenesFormatoTestigos($sRutaImagenDocumento, $sRutaImagenFirmaTestigo)
	{
		//Se crean tres objetos
		$imagenDocumento = new Imagick($sRutaImagenDocumento);
		$imagenFirmaTestigo = new Imagick($sRutaImagenFirmaTestigo);
		CLogImpresion::escribirLog("Ruta del documento-> ".$sRutaImagenDocumento);
		CLogImpresion::escribirLog("Ruta de la imagen Testigo-> ".$sRutaImagenFirmaTestigo);
		//SE ASIGNA LA RESOLUCION DEL PDF
		$imagenDocumento->setResolution(200,200);
		
		//SE LEE EL PDF Y LAS IMAGENES A ADJUNTAR
		$imagenDocumento->readImage($sRutaImagenDocumento);
		$imagenFirmaTestigo->readImage($sRutaImagenFirmaTestigo);
		
		$arrResp = array();
		$arrResp["estado"] = -1;
		
		$imagenDocumento->setImageFormat('jpeg');
		$imagenDocumento->setCompression(Imagick::COMPRESSION_JPEG);
		$imagenDocumento->setCompressionQuality(100);
		
		//Se modifica el tamaño de la firma
		$imagenFirmaTestigo->resizeImage(690, 300, Imagick::FILTER_BOX, 1);
		
		//La segunda imagen se pone arriba de la primera imagen
		$imagenDocumento->compositeImage($imagenFirmaTestigo, Imagick::COMPOSITE_BUMPMAP, 518, 990);
		
		if($imagenDocumento->writeImage($sRutaImagenDocumento))
		{
			CLogImpresion::escribirLog("SE UNIERON IMAGENES CON EXITO");
			$arrResp["estado"] = OK___;
		}
		else{
			CLogImpresion::escribirLog("ERROR AL UNIR FIRMAS");
		}
		
		return $arrResp;
	}

	function my_shell_exec($cmd, &$stdout=null, &$stderr=null)
	{
		$proc = proc_open($cmd,[
			1 => ['pipe','w'],
			2 => ['pipe','w'],
		],$pipes);
		$stdout = stream_get_contents($pipes[1]);
		fclose($pipes[1]);
		$stderr = stream_get_contents($pipes[2]);
		fclose($pipes[2]);
		return proc_close($proc);
	}

	//FUNCION PARA ACTUALIZAR O INSERTAR EN LA TABLA "stmppublicarimagenservicios" Y POSTERIORMENTE SE GENERE LA IMAGEN DEL FORMATO PDF
	function actualizarpublicarimagenservicios($iFolio)
	{
		global $cnxDb;
		$arrResp = array();
		$arrResp["estado"] = -1;
		
		try
		{
			//Se abre una conexion
			$cnxDb =  new PDO( "pgsql:host=".IPSERVIDOR.";port=5432;dbname=".BASEDEDATOS, USUARIO, PASSWORD);
			
			if($cnxDb)
			{
				$cSql = "SELECT fnpublicafirmareenrolservicios ($iFolio);"; 
				self::grabarLogx('[CMetodosExpedienteAfiliacion::actualizarpublicarimagenservicios] Funcion: '.$cSql);
				//Ejecuta la consulta
				$resulSet = $cnxDb->query($cSql);

				if($resulSet)
				{
					$arrResp["estado"] = 1;
					self::grabarLogx('[CMetodosExpedienteAfiliacion::actualizarpublicarimagenservicios] Estado: '.$arrResp["estado"]);
				}
			}
		}
		catch (Exception $e)
		{
			//Cacha la exepcion por la que fallo la ejecucion del query y lo manda como parametro  para escribir la descriccion del error.
			 $mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			 self::grabarLogx('[CMetodosExpedienteAfiliacion::actualizarpublicarimagenservicios]'. $mensaje);
		}
		
		$cnxDb = null;
		return $arrResp;
	}

	function actualizarfirmapublicacion($cFolio,$iOpcion,$iTes)
	{
	
		global $cnxDb;
		$arrResp = array();
		$arrResp["estado"] = -1;
		
		try
		{
			//Se abre una conexion
			$cnxDb =  new PDO( "pgsql:host=".IPSERVIDOR.";port=5432;dbname=".BASEDEDATOS, USUARIO, PASSWORD);
			
			if($cnxDb)
			{
				$cSql = "SELECT fnpublicafirmaenrolamiento ('".$cFolio."',$iOpcion, $iTes);"; 
				self::grabarLogx($cSql);
				//die("$cSql");
				//Ejecuta la consulta
				$resulSet = $cnxDb->query($cSql);

				if($resulSet)
				{
					$arrResp["estado"] = 1;
					self::grabarLogx('[CMetodosExpedienteAfiliacion::actualizarfirmapublicacion]Estado:'.$arrResp["estado"]);
				}
			}
		}
		catch (Exception $e)
		{
			//Cacha la exepcion por la que fallo la ejecucion del query y lo manda como parametro  para escribir la descriccion del error.
			 $mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			 self::grabarLogx('[CMetodosExpedienteAfiliacion::actualizarfirmapublicacion]'. $mensaje);
		}

		$cnxDb = null;
		return $arrResp;
	}

	//FUNCION PARA ACTUALIZAR O INSERTAR EN LA TABLA "stmppublicarimagen" Y POSTERIORMENTE SE GENERE LA IMAGEN DEL FORMATO PDF
	function actualizarpublicarimagen($iFolio)
	{
		global $cnxDb;
		$arrResp = array();
		$arrResp["estado"] = -1;
		
		try
		{
			//Se abre una conexion
			$cnxDb =  new PDO( "pgsql:host=".IPSERVIDOR.";port=5432;dbname=".BASEDEDATOS, USUARIO, PASSWORD);
			
			if($cnxDb)
			{
				$cSql = "SELECT fnpublicafirmareenroltrabajador ($iFolio);"; 
				self::grabarLogx('[CMetodosExpedienteAfiliacion::actualizarpublicarimagen] Funcion: '.$cSql);
				//Ejecuta la consulta
				$resulSet = $cnxDb->query($cSql);
				
				if($resulSet)
				{
					$arrResp["estado"] = 1;
					self::grabarLogx('[CMetodosExpedienteAfiliacion::actualizarpublicarimagen] Estado: '.$arrResp["estado"]);
				}
			}
		}
		catch (Exception $e)
		{
			//Cacha la exepcion por la que fallo la ejecucion del query y lo manda como parametro  para escribir la descriccion del error.
			 $mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			 self::grabarLogx('[CMetodosExpedienteAfiliacion::actualizarpublicarimagen]'. $mensaje);
		}
		
		$cnxDb = null;
		return $arrResp;
	}

	//ACTUALIZA O INSERTA EN LA TABLA "ctrlimagenesportransmitir" Y POSTERIORMENTE SE GENERE Y TRANSMITA EL PDF/IMAGEN AL SERVIDOR BULL DOZER.
	//SUSTITUYE A LA FUNCION actualizarpublicarimagen para que ya no se registre en la tabla "stmppublicarimagen".
	function ctrlImagenesPorTransmitir($iOpcion,$iFolio,$cNombreArchivo)
	{
		CLogImpresion::escribirLog("Entro a ctrlImagenesPorTransmitir (actualizarpublicarimagen).");
		
		try
		{
			$arrResp = array();
			$arrResp['codigorespuesta'] =0;
			$arrResp['descripcion'] ='';
			$i = 0;
			
			//Se abre conexion a BD (aforeglobal).
			$cnxDb =  new PDO( "pgsql:host=".IPSERVIDOR.";port=5432;dbname=".BASEDEDATOS, USUARIO, PASSWORD);
			
			//Verificamos si la conexion se abrio exitosamente.
			if($cnxDb)
			{
				CLogImpresion::escribirLog("Se abrio conexion a BD (aforeglobal).");
				
				$cSql = "SELECT tfolio FROM fnctrlimagenesportransmitir($iOpcion, $iFolio, '', 0, '$cNombreArchivo');";
				
				CLogImpresion::escribirLog("Ejecutada query [ctrlImagenesPorTransmitir (actualizarpublicarimagen)]: $cSql");
				
				$resulSet = $cnxDb->query($cSql);
				if($resulSet)
				{
					foreach($resulSet as $reg)
					{
						$arrResp['tfolio'] = $reg['tfolio'];
						$i++;
					}
					if($i > 0)
					{
						$arrResp['codigorespuesta'] = 1;
						$arrResp['descripcion'] = "Imagen [$cNombreArchivo] registrada en BD para trasmitir correctamente.";
						CLogImpresion::escribirLog($arrResp['descripcion']);
					}
					else
					{
						$arrResp['codigorespuesta'] = 0;
						$arrResp['descripcion'] = "No se registro en BD la imagen para transimitir $cNombreArchivo: $cSql";
						CLogImpresion::escribirLog($arrResp['descripcion']);
					}
				}
				else
				{
					$arrResp['codigorespuesta'] = 0;
					$arrResp['descripcion'] = "Error al ejecutar query [ctrlImagenesPorTransmitir (actualizarpublicarimagen)]: $cSql";
					CLogImpresion::escribirLog($arrResp['descripcion']);
				}
			}
			else
			{
				//Regresa el error al usuario
				$arrResp['codigorespuesta'] = 0;
				$arrResp['descripcion'] = "Error al abrir conexion a BD (aforeglobal) en query [ctrlImagenesPorTransmitir (actualizarpublicarimagen)]: $cSql";
				CLogImpresion::escribirLog($arrResp['descripcion']);
			}
		}
		catch (Exception $e)
		{
			$mensaje= 'Excepcion [ctrlImagenesPorTransmitir (actualizarpublicarimagen)]: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			$arrResp['descripcion'] = $mensaje;
			CLogImpresion::escribirLog($arrResp['descripcion']);
		}
		
		$cnxDb = null;
		CLogImpresion::escribirLog("Se cierra conexion a BD (aforeglobal).");
		
		return $arrResp;
	}
	
}
?> 
