<?php

$configaraciones = simplexml_load_file("../conf/webconfig.xml");
$ipServidor = $configaraciones->Spa;
$sUsuario = $configaraciones->Usuario;
$sBasedeDatos = $configaraciones->Basededatos;
$sPassword = $configaraciones->Password;

$ipServidorInfx = $configaraciones->Ipinformix;
$sUsuarioInfx = $configaraciones->UsuarioInf;
$sBasedeDatosInfx = $configaraciones->BasededatosInf;
$sPasswordInfx = $configaraciones->PasswordInf;


$ipServidorBusTramites = $configaraciones->IpBusTramites;
$sUsuarioBusTramites = $configaraciones->UsuarioBusTramites;
$sBasedeDatosBusTramites = $configaraciones->BasededatosBusTramites;
$sPasswordBusTramites = $configaraciones->PasswordBusTramites;
$sUsaurioPublicaFtp = $configaraciones->UsuarioPublicacionFtp;
$PswdPublicaFtp = $configaraciones->PasswordPublicacionFtp;
$ipPublicacionImagenes = $configaraciones->IpPublicacionImagenes;

//Llena las variables define apartir de la lectura del xml
define("IPSERVIDOR", $ipServidor);
define("BASEDEDATOS", $sBasedeDatos);
define("USUARIO", $sUsuario);
define("PASSWORD", $sPassword);
define('OK___',	1);
define('ERR__',	-1);

define("IPSERVIDORINF", $ipServidorInfx);
define("BASEDEDATOSINF", $sBasedeDatosInfx);
define("USUARIOINF", $sUsuarioInfx);
define("PASSWORDINF", $sPasswordInfx);


define("IPSERVIDORBUSTRAMITES", $ipServidorBusTramites);
define("BASEDEDATOSBUSTRAMITES", $sBasedeDatosBusTramites);
define("USUARIOBUSTRAMITES", $sUsuarioBusTramites);
define("PASSWORDBUSTRAMITES", $sPasswordBusTramites);
define("USUARIO_PUBLICA_FTP", $sUsaurioPublicaFtp);
define("PASS_PUBLICA_FTP", $PswdPublicaFtp);
define("IP_PUBLICACION_IMAGENES",$ipPublicacionImagenes);



?>
