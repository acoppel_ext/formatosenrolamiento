<?php
	include("../fpdf/code39.php");
	include("../fpdi/fpdi.php");
	include("clases/CMetodosExpedienteAfiliacion.php");
	include_once ("clases/CLogImpresion.php");
	date_default_timezone_set('America/Mazatlan');

$objGn = new CMetodosExpedienteAfiliacion();
$cnxBd = null;
$iCount = 0;
$iContinuar = 0;
$iCount1 = 0;
$iContinuar1 = 0;

$arrErr = array();
$arrResp = array();

$cnxBd =  new PDO( "pgsql:host=".IPSERVIDOR.";port=5432;dbname=".BASEDEDATOS, USUARIO, PASSWORD);
if($cnxBd)
{
	CLogImpresion::escribirLog("Accedio a la base de datos");
	if(isset($_GET['enrolado']) && isset($_GET['testigo']) && isset($_GET['numtes']))
	{
		CLogImpresion::escribirLog("Parametro enviado correctamente");
		$iEnrolado =$_GET['enrolado'];
		$itestigo = $_GET['testigo'];
		$intes = $_GET['numtes'];


		$sSql = "";
		$sSql = "SELECT COUNT(*) FROM fnconsultaempleadoenrolamiento(1,".$iEnrolado.")";
		$resulSet = $cnxBd->query($sSql);
		$sSql1 = "";
		$sSql1 = "SELECT COUNT(*) FROM fnconsultaempleadoenrolamientotes(".$itestigo.")";
		$resulSet1 = $cnxBd->query($sSql1);
		if($resulSet)
		{	
			$iCount = 0;		
			$iCount =  $resulSet->fetchColumn();
			if($iCount > 0)
			{
				$iContinuar = 1;
				CLogImpresion::escribirLog("Registros encontrados de enrolado");
			}
		}
		else
		{
			$arrErr = $cnxBd->errorInfo();
			echo ' Error: ' . $arrErr[0] . '-' . $arrErr[1] . '-' . $arrErr[2];
			CLogImpresion::escribirLog(' Error: ' . $arrErr[0] . '-' . $arrErr[1] . '-' . $arrErr[2]);
		}
		if($resulSet1)
		{
			$iCount1 = 0;		
			$iCount1 =  $resulSet1->fetchColumn();
			if($iCount1 > 0)
			{
				$iContinuar1 = 1;
				CLogImpresion::escribirLog("Registros encontrados de testigo");
			}
		}
		else
		{
			$arrErr = $cnxBd->errorInfo();
			echo ' Error: ' . $arrErr[0] . '-' . $arrErr[1] . '-' . $arrErr[2];
			CLogImpresion::escribirLog(' Error: ' . $arrErr[0] . '-' . $arrErr[1] . '-' . $arrErr[2]);
		}

		if($iContinuar > 0 && $iContinuar1 > 0)
		{
			$sSql = "";
			$sSql = "SELECT nombre,apellidopa,apellidoma,curp,dciudad,destado,tipo, nss,clave 
						FROM fnconsultaempleadoenrolamiento(1,".$iEnrolado.");";
			$resulSet = $cnxBd->query($sSql);
			$sSql1 = "";
			$sSql1 = "SELECT nombre,apellidopa,apellidoma,curp, nss 
						FROM fnconsultaempleadoenrolamientotes(".$itestigo.");";
			$resulSet1 = $cnxBd->query($sSql1);
			if($resulSet)
			{	//Se cargan los datos del enrolado
				foreach($resulSet as $reg)
				{
					$arrDatosValidacionEnrolamiento  = array(	'enrolado' => $iEnrolado,
									'testigo' => $itestigo,
									'ntes' => $intes,
									'nombre' => $reg['nombre'],
									'paterno' => $reg['apellidopa'],
									'materno' => $reg['apellidoma'],
									'curp' => $reg['curp'],
									'ciudad' => $reg['dciudad'],
									'estado' => $reg['destado'],
									'tipo' => $reg['tipo'],
									'nss' => $reg['nss'],
									'clave' => $reg['clave'],
									);
					//FORMATO VACIO
				CLogImpresion::escribirLog("Se lleno arreglo correctamente");
				$arrDatosValidacionEnrolamientoNew = array_map('trim', $arrDatosValidacionEnrolamiento);
				}// END FOREACH
			}
			else
			{
				$arrErr = $cnxBd->errorInfo();
				echo ' Error: ' . $arrErr[0] . '-' . $arrErr[1] . '-' . $arrErr[2];
				CLogImpresion::escribirLog(' Error: ' . $arrErr[0] . '-' . $arrErr[1] . '-' . $arrErr[2]);
			}
			if($resulSet1)
			{	
				foreach($resulSet1 as $reg1)
				{
					//se cargan los datos del testigo
					$arrDatosValidacionEnrolamiento1  = array(	'enrolado' => $iEnrolado,
									'testigo' => $itestigo,
									'ntes' => $intes,
									'nombre' => $reg1['nombre'],
									'paterno' => $reg1['apellidopa'],
									'materno' => $reg1['apellidoma'],
									'curp' => $reg1['curp'],
									'nss' => $reg1['nss'],
									);
					//FORMATO VACIO
				CLogImpresion::escribirLog("Se lleno arreglo correctamente");
				$arrDatosValidacionEnrolamientoNew1 = array_map('trim', $arrDatosValidacionEnrolamiento1);
				}// END FOREACH
			}
			else
			{
				$arrErr = $cnxBd->errorInfo();
				echo ' Error: ' . $arrErr[0] . '-' . $arrErr[1] . '-' . $arrErr[2];
				CLogImpresion::escribirLog(' Error: ' . $arrErr[0] . '-' . $arrErr[1] . '-' . $arrErr[2]);
			}
		} //VALIDAR EXISTE FOLIO
	}
	else
	{
		echo 'parametros incorrectos<br>';
		CLogImpresion::escribirLog('parametros incorrectos<br>');

	}

	if($iContinuar > 0 && $iContinuar1 > 0)
	{
			generarValidacionEnrolamiento($arrDatosValidacionEnrolamiento, $arrDatosValidacionEnrolamiento1, $iEnrolado, $itestigo, $intes);
	}
	else
	{
			echo ' Excepcion! El Empleado Solicitado no existe';
			CLogImpresion::escribirLog(' Excepcion! El Empleado Solicitado no existe');
	}
}
else
{
	echo 'Error, al abrir conexion a BD postgres';
	CLogImpresion::escribirLog('Error, al abrir conexion a BD postgres');
}

//Recibe los datos del enrolado y testigo, tambien numero de testigo
function generarValidacionEnrolamiento($arrDatosValidacionEnrolamiento, $arrDatosValidacionEnrolamiento1, $iEnrolado, $itestigo, $intes)
{
	$cnxBd =  new PDO( "pgsql:host=".IPSERVIDOR.";port=5432;dbname=".BASEDEDATOS, USUARIO, PASSWORD);
	CLogImpresion::escribirLog("Entro a generarValidacionEnrolamiento");
	$respuesta = new stdClass();
	$pdf = new PDF_Code39();
	$pdf->AliasNbPages();
	$pdf->AddPage('P','Letter');
	$iAltoRen = 5.5;
	$iSaltoLinea = 5;
	$sizeFontCuerpo = 12;
	$iRadio = 1;
	$iBorder = 0;
	$InicioMargen = 4;
	$FinMargen = 208;
	$SangriaMargen = 11;
	$iPidActual = getmypid();

	$arrayMeses = array('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
   'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');
     

	//Establecer margenes izquierdo, arriba, derecho
	$pdf->SetMargins(20, 20 ,20);

	$pdf->AddFont('Tahoma','','tahoma.php');
	
	$pdf->SetTextColor(0,0,0);

	$pdf->Image('../imagenes/LogoAfore.jpg',135,30,60, 11);
	
	$pdf->SetXY( 0,78);
	$pdf->SetFont('Arial','U',$sizeFontCuerpo-3);
	$pdf->Cell(194, $iAltoRen, trim($arrDatosValidacionEnrolamiento['ciudad']).', '.trim($arrDatosValidacionEnrolamiento['estado']).', '.date('d')." de ".$arrayMeses[date('m')-1]." de ".date('Y'), $iBorder/**/, 0, 'R');

	$pdf->SetXY( 0,60);
	$pdf->SetFont('Arial','B',$sizeFontCuerpo);
	$pdf->Cell( 216, $iAltoRen, 'VALIDACIÓN DE ENROLAMIENTO' , $iBorder/**/, 0, 'C');

	
	$pdf->setX($InicioMargen);
	$pdf->Ln($iSaltoLinea*2+1);
	
	$pdf->SetFont('Tahoma','B',$sizeFontCuerpo-4);
	$pdf->SetXY( 135,40);
	$pdf->SetTextColor(19,35,142);
	$pdf->Cell( 50, $iAltoRen, 'Efectivo para su Retiro' , $iBorder/**/, 0, 'L');

	$pdf->SetTextColor(0,0,0);
	$pdf->SetFont('Arial','',$sizeFontCuerpo-2);

	$pdf->SetXY( $InicioMargen+13,100);
	$pdf->MultiCell(($FinMargen +$InicioMargen) -35, 5,'Yo, _____________________________________________________________________________________,'."\nen pleno uso de mis facultades manifiesto que el Registro Biométrico de las huellas dactilares, asi como la voz, que se capturaron corresponden a __________________________________________________, por lo anterior certifico haber presenciado dicha captura de información en Módulo de Afore y me hago responsable en caso que se detecte un mal enrolamiento doloso.", $iBorder, 'J');
	
	$pdf->SetXY( 81,109);
	$pdf->Cell( 216, $iAltoRen, trim($arrDatosValidacionEnrolamiento['nombre'])." ".trim($arrDatosValidacionEnrolamiento['paterno'])." ".trim($arrDatosValidacionEnrolamiento['materno']), $iBorder, 0, 'L');

	$pdf->SetXY( 25,99);
	$pdf->Cell( 216, $iAltoRen, trim($arrDatosValidacionEnrolamiento1['nombre'])." ".trim($arrDatosValidacionEnrolamiento1['paterno'])." ".trim($arrDatosValidacionEnrolamiento1['materno'])." - ".trim($arrDatosValidacionEnrolamiento1['curp']), $iBorder, 0, 'L');

	$pdf->SetXY( 0,145);
	$pdf->Cell( 216, $iAltoRen, '________________________________' , $iBorder/**/, 0, 'C');
	
	$pdf->SetXY( 0,151);
	$pdf->Cell( 216, $iAltoRen, 'Firma Testigo '.($arrDatosValidacionEnrolamiento['ntes']) , $iBorder/**/, 0, 'C');
	$caracteres = strlen(trim($arrDatosValidacionEnrolamiento['clave']));
	if(trim($arrDatosValidacionEnrolamiento['tipo'])== 'PROM' AND $caracteres = 9){
		$clav = '0'. trim($arrDatosValidacionEnrolamiento['clave']);
	}
	else {
		$clav = '0000000000';
	}
	$folioTes = trim($arrDatosValidacionEnrolamiento['tipo']).'_'.trim($arrDatosValidacionEnrolamiento['nss'])."_".$clav."TES".($arrDatosValidacionEnrolamiento['ntes']);
	
	$rutapdf = "/sysx/progs/web/salida/formatosenrolamiento/".$folioTes.".pdf";
	$sRutaAbsoluta = $folioTes.".pdf";
	$respuesta->rutapdf = "/sysx/progs/web/salida/formatosenrolamiento/".$folioTes.".pdf";
	$rutaPdfTemp = "/sysx/progs/web/salida/formatosenrolamiento/".$folioTes."_TEMP.pdf";
	//RUTA DE LA FIRMA DEL TESTIGO
	$sRutaFirmaTestigo = "/sysx/progs/web/entrada/firmas/TES".($arrDatosValidacionEnrolamiento1['ntes']).'_'.trim($arrDatosValidacionEnrolamiento['tipo']).'_'.trim($arrDatosValidacionEnrolamiento['nss'])."_FTES".($arrDatosValidacionEnrolamiento1['ntes']).".JPG";		
	
		$pdf->Output($rutapdf);
		$objMetodosExpedienteAfiliacion = new CMetodosExpedienteAfiliacion();		
		copy($rutapdf, $rutaPdfTemp);
		$pdf = new FPDI();
		
		CLogImpresion::escribirLog("Se Abre PDF(Validacion de Enrolamiento)");

			chmod($rutapdf, 0777);
			unset($arrResp);
			$arrResp = $objMetodosExpedienteAfiliacion->unirImagenesFormatoTestigos($rutapdf, $sRutaFirmaTestigo);
		if($arrResp["estado"] == OK___)
		{
			$pageCount = $pdf->setSourceFile('/sysx/progs/web/salida/formatosenrolamiento/'.$sRutaAbsoluta);
			$tplIdx = $pdf->importPage(1);
			$pdf->addPage();
			$pdf->useTemplate($tplIdx, null, null, 0, 0, true);

			CLogImpresion::escribirLog("Se Agregan las firmas al PDF");
			//$pdf->Image($sRutaFirmaTestigo, 77, 123.95, 63, 25, 'JPG');

			//SE GUARDA PDF Y SE MUESTRA EN EL NAVEGADOR
			$pdf->Output($rutapdf, 'F');
			CLogImpresion::escribirLog("Se guarda y se muestra PDF(Validacion de Enrolamiento) en el navegador");
			//Muestra el PDF generado
			//mostrarPDF($rutapdf, $sRutaAbsoluta);
		}
		$cFolio = trim($arrDatosValidacionEnrolamiento['tipo']).'_'.trim($arrDatosValidacionEnrolamiento['nss']);
		$iTes = $arrDatosValidacionEnrolamiento1['ntes'];
		unset($arrResp);
		//SE ACTUALIZA EN LA TABLA STMPPUBLICARIMAGENENROLPROM
		$arrResp = $objMetodosExpedienteAfiliacion->actualizarfirmapublicacion($cFolio,2,$iTes);
		
		//SE EJECUTA LA FUNCION
			$sSql = "";
			$sSql = "SELECT fnactualizarimagenindexada AS Dato FROM fnactualizarimagenindexada('".$cFolio."','".$folioTes.".tif','".obtIpTienda()."');";

			$cnxBd->query($sSql);

}

	//Funcion utilizada para mostrar el pdf del contrato
	function mostrarPDF($rutapdf, $sRutaAbsoluta)
	{
		CLogImpresion::escribirLog("Muestra PDF(Validacion de Enrolamiento)");
		header('Content-Description: File Transfer');
		header('Content-Type: application/pdf');
		header('Content-Disposition: inline; filename="' . basename($sRutaAbsoluta).'"');
		header('Content-Transfer-Encoding: binary');
		header('Expires: 0');
		header('Cache-Control: must-revalidate');
		header('Pragma: public');
		header('Content-Length: ' . filesize($rutapdf));	
		readfile($rutapdf);
	}
	function obtIpTienda()
	{
		$ipCliente = "";
		
		if(isset($_SERVER["HTTP_X_FORWARDED_FOR"]) )
			$ipCliente = $_SERVER['HTTP_X_FORWARDED_FOR']; 
		else
			$ipCliente = $_SERVER['REMOTE_ADDR'];

		return $ipCliente;
	} 


?>