<?php
	include("../fpdf/code39.php");
	include("../fpdi/fpdi.php");
	include("clases/CMetodosExpedienteAfiliacionMovil.php");
	include_once ("clases/CLogImpresion.php");
	date_default_timezone_set('America/Mazatlan');

	$objGn = new CMetodosExpedienteAfiliacionMovil();
	$cnxBd = null;
	$iContinuar = 0;

	$arrErr = array();
	$arrRuta = array("estatus" => 0, "descripcion" => '', "rutapdf" => '');

	$cnxBd =  new PDO( "pgsql:host=".IPSERVIDOR.";port=5432;dbname=".BASEDEDATOS, USUARIO, PASSWORD);
	if($cnxBd)
	{
		CLogImpresion::escribirLog("Accedio a la base de datos");
		$iFolioEnrolamiento = isset($_GET['folioenrolamiento']) ? $_GET['folioenrolamiento'] : 0;

		if($iFolioEnrolamiento > 0)
		{
			CLogImpresion::escribirLog("Parametro enviado correctamente");

			$sSql = "";
			$sSql = "SELECT folio FROM fnconsultatrabajadorenrolamientomovil(".$iFolioEnrolamiento.");";

			$resulSet = $cnxBd->query($sSql);
			if($resulSet)
			{
				$row = $resulSet->fetch(PDO::FETCH_ASSOC);
				CLogImpresion::escribirLog("Folio encontrado: ".$row['folio']);

				if( $row['folio'] > 0 )
				{
					$iContinuar = 1;
				}
			}
			else
			{
				$arrErr = $cnxBd->errorInfo();
				$arrRuta["descripcion"] = 'Error, favor de intentarlo de nuevo. Si el problema persiste, comunicarse con mesa de ayuda';
				CLogImpresion::escribirLog(' Error: ' . $arrErr[0] . '-' . $arrErr[1] . '-' . $arrErr[2]);
			}
			
			if($iContinuar > 0) //VALIDAR EXISTE FOLIO
			{
				$sSql = "";
				$sSql = "SELECT folio, tiposolicitud, curp, nombre, apellidopa, apellidoma, texto 
						 FROM fnconsultatrabajadorenrolamientomovil(".$iFolioEnrolamiento.");";

				$resulSet = $cnxBd->query($sSql);
				if($resulSet)
				{	
					$reg = $resulSet->fetch(PDO::FETCH_ASSOC);

					$arrDatosFormatoEnrolamiento  = array(
									'folio' => $reg['folio'],
									'tiposolicitud' => $reg['tiposolicitud'],
									'curp' => $reg['curp'],
									'nombre' => strpos($reg['nombre'], '�') || strpos($reg['nombre'], '�') ? $reg['nombre'] : utf8_decode($reg['nombre']),
									'paterno' => strpos($reg['apellidopa'], '�') || strpos($reg['apellidopa'], '�') ? $reg['apellidopa'] : utf8_decode($reg['apellidopa']),
									'materno' => strpos($reg['apellidoma'], '�') || strpos($reg['apellidoma'], '�') ? $reg['apellidoma'] : utf8_decode($reg['apellidoma']),
									'texto' => strpos($reg['texto'], '�') || strpos($reg['texto'], '�') ? $reg['texto'] : utf8_decode($reg['texto']),
									);
									
					CLogImpresion::escribirLog("Se lleno arreglo correctamente");
					$arrRuta["rutapdf"] = generarFormatoEnrolamientoTrabajador($arrDatosFormatoEnrolamiento);

					if($arrRuta["rutapdf"] != '') //VALIDAR QUE EL NOMBRE DEL FORMATO CONTENGA ALGO
						$arrRuta["estatus"] = 1;
				}
				else
				{
					$arrErr = $cnxBd->errorInfo();
					$arrRuta["descripcion"] = 'Error, favor de intentarlo de nuevo. Si el problema persiste, comunicarse con mesa de ayuda';
					CLogImpresion::escribirLog(' Error: ' . $arrErr[0] . '-' . $arrErr[1] . '-' . $arrErr[2]);
				}
				
			} //VALIDAR EXISTE FOLIO
			else
			{
				$arrRuta["descripcion"] = 'Error: El Trabajador Solicitado no existe';
				CLogImpresion::escribirLog('Error: El Trabajador Solicitado no existe');
			}
		}
		else
		{
			$arrRuta["descripcion"] = 'Parametros incorrectos';
			CLogImpresion::escribirLog('Parametros incorrectos');
		}
		
	}
	else
	{
		$arrRuta["descripcion"] = 'Error, favor de intentarlo de nuevo. Si el problema persiste, comunicarse con mesa de ayuda';
		CLogImpresion::escribirLog('Error, al abrir conexion a BD postgres');
	}

	echo json_encode($arrRuta);
	
	//METODO PARA GENERAR EL FORMATO DE AUTORIZACION DE HUELLAS DACTILARES EN PDF
	function generarFormatoEnrolamientoTrabajador($arrDatosFormatoEnrolamiento)
	{
		$cnxBd =  new PDO( "pgsql:host=".IPSERVIDOR.";port=5432;dbname=".BASEDEDATOS, USUARIO, PASSWORD);
		$respuesta = new stdClass();
		$pdf = new PDF_Code39();
		$pdf->AliasNbPages();
		$pdf->AddPage('P','Letter');
		$iAltoRen = 5.5;
		$iSaltoLinea = 5;
		$sizeFontCuerpo = 12;
		$iRadio = 1;
		$iBorder = 0;
		$InicioMargen = 4;
		$FinMargen = 208;
		$SangriaMargen = 11;
		$iPidActual = getmypid();
		$iTipoSolicitud = 0;
		global $arrRuta;
		$arrayMeses = array('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
	   'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');

		CLogImpresion::escribirLog("Entro a generarFormatoEnrolamientoTrabajador");

		//Establecer margenes izquierdo, arriba, derecho
		$pdf->SetMargins(20, 20 ,20);
		//Establecemos el margen inferior:
		$pdf->SetAutoPageBreak(true,0);

		$pdf->AddFont('Tahoma','','tahoma.php');
		
		$pdf->SetTextColor(0,0,0);
		$pdf->Image('../imagenes/LogoAfore.jpg',17,55,60,14);
		$pdf->SetTextColor(18,50,137);
		$pdf->SetXY(17,67);
		$pdf->SetFont('Arial','B',$sizeFontCuerpo-3.8);
		$pdf->Cell( 216, $iAltoRen, 'Efectivo para su retiro' , $iBorder, 0, 'L');
		$pdf->SetTextColor(19,111,191);
		$pdf->SetXY( 9,20);
		$pdf->SetFont('Arial','B',$sizeFontCuerpo+4);
		$pdf->Cell( 216, $iAltoRen, 'Formato de Enrolamiento con la firma manuscrita digital' , $iBorder, 0, 'L');
		$pdf->RoundedRect(10, 40, 196, 44, 5, 'D','1,2,3,4' );
		$pdf->RoundedRect(10, 90, 196, 116, 5, 'D','1,2,3,4' );

		$pdf->setX($InicioMargen);
		$pdf->Ln($iSaltoLinea*2+1);
		
		$pdf->SetFont('Tahoma','B',$sizeFontCuerpo-1);
		$pdf->SetXY(87,53);
		$pdf->SetTextColor(0,0,0);
		$pdf->Cell( 57, $iAltoRen, 'Clave de la administradora' , $iBorder, 0, 'L');
		$pdf->SetXY(87,68);
		$pdf->Cell( 57, $iAltoRen, 'Nombre de la administradora' , $iBorder, 0, 'L');
		$pdf->SetXY(147,53);
		$pdf->SetFont('Tahoma','',$sizeFontCuerpo+1);
		$pdf->Cell( 57, $iAltoRen, '568' , $iBorder, 0, 'L');
		$pdf->SetXY(147,68);
		$pdf->Cell( 57, $iAltoRen, 'Afore Coppel' , $iBorder, 0, 'L');
		$pdf->SetFont('Tahoma','B',$sizeFontCuerpo-1);
		$pdf->SetXY(17,105);
		$pdf->Cell( 57, $iAltoRen, 'CURP del Trabajador' , $iBorder, 0, 'L');
		$pdf->SetXY(17,120);
		$pdf->Cell( 57, $iAltoRen, 'Nombre del Trabajador' , $iBorder, 0, 'L');
		$pdf->SetXY(17,135);
		$pdf->Cell( 57, $iAltoRen, 'Fecha del Proceso de la Solicitud' , $iBorder, 0, 'L');
		$pdf->SetTextColor(0,0,0);
		$pdf->SetFont('Arial','',$sizeFontCuerpo-4);
		$pdf->SetXY( $InicioMargen+13,147);
		$pdf->MultiCell(($FinMargen +$InicioMargen) -35, 5,trim($arrDatosFormatoEnrolamiento['texto']), $iBorder, 'J');
		$pdf->SetFont('Arial','',$sizeFontCuerpo);
		$pdf->SetXY( 100,120);
		$pdf->Cell( 216, $iAltoRen, trim($arrDatosFormatoEnrolamiento['nombre'])." ".trim($arrDatosFormatoEnrolamiento['paterno'])." ".trim($arrDatosFormatoEnrolamiento['materno']) , $iBorder, 0, 'L');
		$pdf->SetXY( 100,106);
		$pdf->Cell( 216, $iAltoRen, trim($arrDatosFormatoEnrolamiento['curp']) , $iBorder, 0, 'L');
		$pdf->SetXY( 100,135);
		$pdf->Cell(39, $iAltoRen, date('d')." de ".$arrayMeses[date('m')-1]." de ".date('Y'), $iBorder, 0, 'L');

		$pdf->SetXY( 145,235);
		$pdf->Cell( 52, $iAltoRen, '______________________' , $iBorder, 0, 'L');

		$pdf->SetXY( 150,241);
		$pdf->SetFont('Arial','B',$sizeFontCuerpo-2);
		$pdf->Cell( 43, $iAltoRen, 'Firma manuscrita digital' , $iBorder, 0, 'L');

		//BORDES
		$pdf->SetFillColor(8,52,99);
		$pdf->SetXY( 0,0);
		$pdf->Cell( 216, $iAltoRen-.5, '' , 1, 0, 'C',true);
		$pdf->SetFillColor(8,52,99);
		$pdf->SetXY( 0,274.3);
		$pdf->Cell( 216, $iAltoRen-.5, '' , 1, 0, 'C',true);
		
		$iTipoSolicitud = $arrDatosFormatoEnrolamiento['tiposolicitud'];
		CLogImpresion::escribirLog("Folio = ".$arrDatosFormatoEnrolamiento['folio']);
		CLogImpresion::escribirLog("Tipo Solicitud = ".$iTipoSolicitud);
		
		if($iTipoSolicitud != 26 && $iTipoSolicitud != 27 && $iTipoSolicitud != 33)
		{
			$arrDatosFormatoEnrolamiento['folio'] = $arrDatosFormatoEnrolamiento['folio']."-S";
			CLogImpresion::escribirLog("Es un folio Servicio = ".$arrDatosFormatoEnrolamiento['folio']);
		}
		
		$sNombrePdfTemp = $arrDatosFormatoEnrolamiento['folio']."_".$iPidActual."_FormatoEnrolTrab".trim($arrDatosFormatoEnrolamiento['curp'])."_TEMP.pdf";
		$rutaPdfTemp = "/sysx/progs/web/salida/formatosenrolamiento/".$sNombrePdfTemp;
		
		$objMetodosExpedienteAfiliacionMovil = new CMetodosExpedienteAfiliacionMovil();
		
		$pdf->Output($rutaPdfTemp);
		CLogImpresion::escribirLog("Se guarda PDF sin firma");
		
		//SE PASAN LOS DATOS A LA FUNCION ctrlImagenesPorTransmitir.
		$iOpcion = 6; //6 [FAHD,FTE0] Formato de Enrolamiento
		$iFolio = $arrDatosFormatoEnrolamiento['folio'];
		$cNombreArchivo = $arrDatosFormatoEnrolamiento['folio']."_".$iPidActual."_FormatoEnrolTrab".trim($arrDatosFormatoEnrolamiento['curp'])."_TEMP.pdf";
		
		//SE ACTUALIZA EN LA TABLA "ctrlImagenesPorTransmitir".
		$arrResp = $objMetodosExpedienteAfiliacionMovil->ctrlImagenesPorTransmitir($iOpcion,$iFolio,$cNombreArchivo);
		CLogImpresion::escribirLog("Se registraron los datos del PDF en ctrlImagenesPorTransmitir correctamente.");
		
		return $sNombrePdfTemp;
	}
	
?>