
<?php
	include("../fpdf/code39.php");
	include("../fpdi/fpdi.php");
	include("clases/CMetodosExpedienteAfiliacion.php");
	include_once ("clases/CLogImpresion.php");
	date_default_timezone_set('America/Mazatlan');


$objGn = new CMetodosExpedienteAfiliacion();
$cnxBd = null;
$iCount = 0;
$iContinuar = 0;

$arrErr = array();
$arrResp = array();

$cnxBd =  new PDO( "pgsql:host=".IPSERVIDOR.";port=5432;dbname=".BASEDEDATOS, USUARIO, PASSWORD);
if($cnxBd)
{
	CLogImpresion::escribirLog("Accedio a la base de datos");
	if(isset($_GET['enrolado']))
	{
		CLogImpresion::escribirLog("Parametro enviado correctamente");
		$iEnrolado =$_GET['enrolado'];
		//$folio = $_GET['folio'];

		$sSql = "";
		$sSql = "SELECT COUNT(*) FROM fnconsultaempleadoenrolamiento(1,".$iEnrolado.")";
		$resulSet = $cnxBd->query($sSql);
		if($resulSet)
		{	
			$iCount = 0;		
			$iCount =  $resulSet->fetchColumn();
			if($iCount > 0)
			{
				$iContinuar = 1;
				CLogImpresion::escribirLog("Registros encontrados");
			}
		}
		else
		{
			$arrErr = $cnxBd->errorInfo();
			echo ' Error: ' . $arrErr[0] . '-' . $arrErr[1] . '-' . $arrErr[2];
			CLogImpresion::escribirLog(' Error: ' . $arrErr[0] . '-' . $arrErr[1] . '-' . $arrErr[2]);
		}
		if($iContinuar > 0)
		{
			$sSql = "";
			$sSql = "SELECT nombre,apellidopa,apellidoma,curp, dciudad, destado, nss, tipo, clave, texto 
						FROM fnconsultaempleadoenrolamiento(1, ".$iEnrolado.");";

			$resulSet = $cnxBd->query($sSql);
			if($resulSet)
			{	
				//Se cargan los datos del enrolado
				foreach($resulSet as $reg)
				{
					$arrDatosFormatoEnrolamiento  = array(	'enrolado' => $iEnrolado,
									'nombre' => $reg['nombre'],
									'paterno' => $reg['apellidopa'],
									'materno' => $reg['apellidoma'],
									'curp' => $reg['curp'],
									'ciudad' => $reg['dciudad'],
									'estado' => $reg['destado'],
									'nss' => $reg['nss'],
									'tipo' => $reg['tipo'],
									'clave' => $reg['clave'],
									'texto' => $reg['texto'],
									);
				//FORMATO VACIO
				CLogImpresion::escribirLog("Se lleno arreglo correctamente");
				$arrDatosFormatoEnrolamientoNew = array_map('trim', $arrDatosFormatoEnrolamiento);
				}// END FOREACH
			}
			else
			{
				$arrErr = $cnxBd->errorInfo();
				echo ' Error: ' . $arrErr[0] . '-' . $arrErr[1] . '-' . $arrErr[2];
				CLogImpresion::escribirLog(' Error: ' . $arrErr[0] . '-' . $arrErr[1] . '-' . $arrErr[2]);
			}
		} //VALIDAR EXISTE FOLIO
	}
	else
	{
		echo 'parametros incorrectos<br>';
		CLogImpresion::escribirLog('parametros incorrectos<br>');

	}

	if($iContinuar > 0)
	{
			$arrRuta = array("rutapdf");
			$arrRuta["rutapdf"] =generarFormatoEnrolamiento($arrDatosFormatoEnrolamiento, $iEnrolado);
			echo json_encode($arrRuta);
	}
	else
	{
			echo ' Excepcion! El Empleado Solicitado no existe';
			CLogImpresion::escribirLog(' Excepcion! El Empleado Solicitado no existe');
	}
}
else
{
	echo 'Error, al abrir conexion a BD postgres';
	CLogImpresion::escribirLog('Error, al abrir conexion a BD postgres');
}

//Recibe nombre del enrolado, numero de empleado de enrolado.
function generarFormatoEnrolamiento($arrDatosFormatoEnrolamiento, $iEnrolado)
{
	$cnxBd =  new PDO( "pgsql:host=".IPSERVIDOR.";port=5432;dbname=".BASEDEDATOS, USUARIO, PASSWORD);
	$respuesta = new stdClass();
	$pdf = new PDF_Code39();
	$pdf->AliasNbPages();
	$pdf->AddPage('P','Letter');
	$iAltoRen = 5.5;
	$iSaltoLinea = 5;
	$sizeFontCuerpo = 12;
	$iRadio = 1;
	$iBorder = 0;
	$InicioMargen = 4;
	$FinMargen = 208;
	$SangriaMargen = 11;
	$iPidActual = getmypid();

	$arrayMeses = array('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
   'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');

	CLogImpresion::escribirLog("Entro a generarFormatoEnrolamiento");

	//Decodificar el arreglo para mostrar correctamente los acentos
	foreach ($arrDatosFormatoEnrolamiento as $key=>$valor)
	{
		$arrDatosFormatoEnrolamiento[$key] = $arrDatosFormatoEnrolamiento[$key];
	}
	//Establecer margenes izquierdo, arriba, derecho
	$pdf->SetMargins(20, 20 ,20);
	//Establecemos el margen inferior:
	$pdf->SetAutoPageBreak(true,0);

	$pdf->AddFont('Tahoma','','tahoma.php');
	
	$pdf->SetTextColor(0,0,0);
	$pdf->Image('../imagenes/LogoAfore.jpg',17,55,60,14);
	$pdf->SetTextColor(18,50,137);
	$pdf->SetXY(17,67);
	$pdf->SetFont('Arial','B',$sizeFontCuerpo-3.8);
	$pdf->Cell( 216, $iAltoRen, 'Efectivo para su retiro' , $iBorder, 0, 'L');
	$pdf->SetTextColor(19,111,191);
	$pdf->SetXY( 9,20);
	$pdf->SetFont('Arial','B',$sizeFontCuerpo+4);
	$pdf->Cell( 216, $iAltoRen, 'Formato de Enrolamiento con la firma manuscrita digital' , $iBorder, 0, 'L');
	$pdf->RoundedRect(10, 40, 196, 44, 5, 'D','1,2,3,4' );
	$pdf->RoundedRect(10, 90, 196, 116, 5, 'D','1,2,3,4' );

	$pdf->setX($InicioMargen);
	$pdf->Ln($iSaltoLinea*2+1);
	
	$pdf->SetFont('Tahoma','B',$sizeFontCuerpo-1);
	$pdf->SetXY(87,53);
	$pdf->SetTextColor(0,0,0);
	$pdf->Cell( 57, $iAltoRen, 'Clave de la administradora' , $iBorder, 0, 'L');
	$pdf->SetXY(87,68);
	$pdf->Cell( 57, $iAltoRen, 'Nombre de la administradora' , $iBorder, 0, 'L');
	$pdf->SetXY(147,53);
	$pdf->SetFont('Tahoma','',$sizeFontCuerpo+1);
	$pdf->Cell( 57, $iAltoRen, '568' , $iBorder, 0, 'L');
	$pdf->SetXY(147,68);
	$pdf->Cell( 57, $iAltoRen, 'Afore Coppel' , $iBorder, 0, 'L');
	$pdf->SetFont('Tahoma','B',$sizeFontCuerpo-1);
	$pdf->SetXY(17,105);
	$pdf->Cell( 57, $iAltoRen, 'CURP del Trabajador' , $iBorder, 0, 'L');
	$pdf->SetXY(17,120);
	$pdf->Cell( 57, $iAltoRen, 'Nombre del Trabajador' , $iBorder, 0, 'L');
	$pdf->SetXY(17,135);
	$pdf->Cell( 57, $iAltoRen, 'Fecha del Proceso de la Solicitud' , $iBorder, 0, 'L');
	$pdf->SetTextColor(0,0,0);
	$pdf->SetFont('Arial','',$sizeFontCuerpo-4);
	$pdf->SetXY( $InicioMargen+13,147);
	$pdf->MultiCell(($FinMargen +$InicioMargen) -35, 5,utf8_decode(trim($arrDatosFormatoEnrolamiento['texto'])), $iBorder, 'J');
	$pdf->SetFont('Arial','',$sizeFontCuerpo);
	$pdf->SetXY( 100,120);
	$pdf->Cell( 216, $iAltoRen, trim($arrDatosFormatoEnrolamiento['nombre'])." ".trim($arrDatosFormatoEnrolamiento['paterno'])." ".trim($arrDatosFormatoEnrolamiento['materno']) , $iBorder, 0, 'L');
	$pdf->SetXY( 100,106);
	$pdf->Cell( 216, $iAltoRen, trim($arrDatosFormatoEnrolamiento['curp']) , $iBorder, 0, 'L');
	$pdf->SetXY( 100,135);
	$pdf->Cell(39, $iAltoRen, /*utf8_decode(trim($arrDatosFormatoEnrolamiento['ciudad'])).', '.utf8_decode(trim($arrDatosFormatoEnrolamiento['estado'])).', '.*/date('d')." de ".$arrayMeses[date('m')-1]." de ".date('Y'), $iBorder/**/, 0, 'L');


	$pdf->SetXY( 145,235);
	$pdf->Cell( 52, $iAltoRen, '______________________' , $iBorder, 0, 'L');

	$pdf->SetXY( 150,241);
	$pdf->SetFont('Arial','B',$sizeFontCuerpo-2);
	$pdf->Cell( 43, $iAltoRen, 'Firma manuscrita digital' , $iBorder, 0, 'L');

	//BORDES
	$pdf->SetFillColor(8,52,99);
	$pdf->SetXY( 0,0);
	$pdf->Cell( 216, $iAltoRen-.5, '' , 1, 0, 'C',true);
	$pdf->SetFillColor(8,52,99);
	$pdf->SetXY( 0,274.3);
	$pdf->Cell( 216, $iAltoRen-.5, '' , 1, 0, 'C',true);
	$caracteres = strlen(trim($arrDatosFormatoEnrolamiento['clave']));
	if($arrDatosFormatoEnrolamiento['tipo'] == 'PROM'){
		if($caracteres = 9){
		$clav = '0'.trim($arrDatosFormatoEnrolamiento['clave']);
		}
	}
	else if ($arrDatosFormatoEnrolamiento['tipo'] == 'POST'){
		$clav = '0000000000';
	}
	
		$fol= "_".$clav."FAHD";
		$folio =  $arrDatosFormatoEnrolamiento['tipo']."_".$arrDatosFormatoEnrolamiento['nss'].$fol;
		$rutapdf = "/sysx/progs/web/salida/formatosenrolamiento/".$folio.".pdf";
		$rutaa = "/salida/formatosenrolamiento/".$folio.".pdf";
		$sRutaAbsoluta = $folio.".pdf";
		$respuesta->rutapdf = "/sysx/progs/web/salida/formatosenrolamiento/".$folio.".pdf";
		$rutaPdfTemp = "/sysx/progs/web/salida/formatosenrolamiento/".$folio."_TEMP.pdf";
	
	//RUTA DE LA FIRMA DEL ENROLADO
	$sRutaFirmaEnrolado = "/sysx/progs/web/entrada/firmas/FAHD_".($arrDatosFormatoEnrolamiento['tipo'])."_".($arrDatosFormatoEnrolamiento['nss'])."_FENRO.JPG";		
	
			$pdf->Output($rutapdf);
			$objMetodosExpedienteAfiliacion = new CMetodosExpedienteAfiliacion();	
			CLogImpresion::escribirLog("Se Abre PDF(Formato de enrolamiento)");
			copy($rutapdf, $rutaPdfTemp);
			$pdf = new FPDI();

			chmod($rutapdf, 0777);
			unset($arrResp);
			$arrResp = $objMetodosExpedienteAfiliacion->unirImagenesFormatoEnrol($rutapdf, $sRutaFirmaEnrolado);
			if($arrResp["estado"] == OK___)
			{
			    $pageCount = $pdf->setSourceFile('/sysx/progs/web/salida/formatosenrolamiento/'.$sRutaAbsoluta);
				$tplIdx = $pdf->importPage(1);
				$pdf->addPage();
				$pdf->useTemplate($tplIdx, null, null, 0, 0, true);
				
				CLogImpresion::escribirLog("Se Agregan las firmas al PDF");

				//$pdf->Image($sRutaFirmaEnrolado, 143.35, 219.4, 57, 20, 'JPG');
				//$pdf->SetXY( 143.35,219.4);
				//$pdf->Cell( 57, 20, '' , 1, 0, 'L');
				
				//SE GUARDA PDF Y SE MUESTRA EN EL NAVEGADOR
				$pdf->Output($rutapdf, 'F');
				CLogImpresion::escribirLog("Se guarda y se muestra PDF(Forma to de Enrolamiento) en el navegador");
				//Muestra el PDF generado
				//mostrarPDF($rutapdf, $sRutaAbsoluta);
			}
			
			$cFolio = $arrDatosFormatoEnrolamiento['tipo'].'_'.trim($arrDatosFormatoEnrolamiento['nss']);
			unset($arrResp);
			//SE ACTUALIZA EN LA TABLA STMPPUBLICARIMAGENENROLPROM
			$arrResp = $objMetodosExpedienteAfiliacion->actualizarfirmapublicacion($cFolio,1,0);
			//SE EJECUTA LA FUNCION
			$sSql = "";
			$sSql = "SELECT fnactualizarimagenindexada AS Dato FROM fnactualizarimagenindexada('".$cFolio."','".$folio.".tif','".obtIpTienda()."');";

			$cnxBd->query($sSql);
			return $rutaa;
}

	//Funcion utilizada para mostrar el pdf del contrato
	function mostrarPDF($rutapdf, $sRutaAbsoluta)
	{
		header('Content-Description: File Transfer');
		header('Content-Type: application/pdf');
		header('Content-Disposition: inline; filename="' . basename($sRutaAbsoluta).'"');
		header('Content-Transfer-Encoding: binary');
		header('Expires: 0');
		header('Cache-Control: must-revalidate');
		header('Pragma: public');
		header('Content-Length: ' . filesize($rutapdf));	
		readfile($rutapdf);
		CLogImpresion::escribirLog("Muestra PDF(Formato de Enrolamiento)");
	}


	function obtIpTienda()
	{
		$ipCliente = "";
		
		if(isset($_SERVER["HTTP_X_FORWARDED_FOR"]) )
			$ipCliente = $_SERVER['HTTP_X_FORWARDED_FOR']; 
		else
			$ipCliente = $_SERVER['REMOTE_ADDR'];

		return $ipCliente;
	} 


?>